# @summary
#   This class handles the postfix service.
#
# @api private
#
class postfix::service inherits postfix {

  if ! ($postfix::service_ensure in [ 'running', 'stopped' ]) {
    fail('service_ensure parameter must be running or stopped')
  }
  if $postfix::service_manage == true {
    service { $postfix::service_name:
      ensure     => $postfix::service_ensure,
      enable     => $postfix::service_enable,
      provider   => systemd,
      hasstatus  => true,
      hasrestart => true,
    }
  }

}
