# @summary
#   This class handles postfix packages.
#
# @api private
#
class postfix::install inherits postfix {

  $postfix::postfix_packages.each |String $package|{
    package { $package:
      ensure => installed,
    }
  }

}
