# postfix
#
# @summary Init class for postfix module, includes other classes
#
# @example Declare this class in site.pp
#   class postfix {
#   configuration_directory => '/usr/local/postfix/etc/postfix',
#   package_name            => postfix',
#   service_name            => 'postfix',
#   }
#
# @param service_name
#   The real service name's. Default value: undef.
#
# @param service_enable
#   true or false. Default value: false.
#
# @param mail_name
#   Postfix public signature. Default value: undef.
#
# @param service_ensure
#   "running" ou "stopped"
#
# @param service_manage
#   true or false. Default value: true.
#
# @param postfix_packages
#   List of postfix packages. Array. Default value: see data/os/*.
#
# @param configuration_directory
#   Default directory, distribution depend. string. Default value: '/etc/postfix/'.
#
# @param postfix_files_directory
#   Files into $postfix_files_directory. string. Default value: 'postfix-files.d/'.
#
# @param sasl_files_directory
#   sasl files directory. string. Default value: 'sasl_files_directory'.
#
# @param sasl_files_full_directory
#   . String. Default value: ''.
#
# @param postfix_files_full_directory
#   . String. Default value: ''.
#
#   sasl files directory. string. Default value: 'sasl_files_directory'.
#
# @param config_files
#   Debian specifics files. Array. Default value: see data/common.yml.
#
# @param access_files
#   Access control files, flat files. Array. Default value: see data/common.yml.
#
# @param db_files
#   Access control files. Array. Default value: see data/common.yml.
#
# @param postfix_files_content
#   Files into $postfix_files_directory. Array. Default value: see data/common.yml.
#
# @param sasl_files
#   Files into $sasl_files_directory. Array. Default value: see data/common.yml.
#
# @param mail_name
#   ehlo name. String[optional). Default value: Undef.
#
# @param smtpd_banner
#   smtpd banner. String[optional]. Default value: Undef.
#
# @param myhostname
#   myhostname must matching with spf record. String[optional]. Default value: Undef.
#
# @param mydomain
#   mydomain must matching with spf record. String[optional]. Default value: Undef.
#
# @param smtp_helo_name
#   smtp_helo_name must matching with spf record. String[optional]. Default value: Undef.
#
# @param enable_debug
#   enable global debug. Boolean. Default value: Undef.
#
# @param debug_peer_list
#   list of domains to debug. String. Default value: Undef.
#
# @param soft_bounce
#   replace bounce 5* by 4*. String. Default value: Undef.
#
# @param delay_warning_time
#   delay_warning_time. String. Default value: Undef.
#
# @param enable_server_ssl
#   enable_server_ssl. Boolean. Default value: Undef.
#
# @param smtpd_tls_cert_file
#   smtpd_tls_cert_file. String. Default value: Undef.
#
# @param smtpd_tls_security_level
#   . String. Default value: Undef.
#
# @param smtpd_tls_key_file
#   smtpd_tls_key_file. String. Default value: Undef.
#
# @param smtpd_use_tls
#   smtpd_use_tls. String. Default value: Undef.
#
# @param smtpd_tls_session_cache_database
#   smtpd_tls_session_cache_database. String. Default value: Undef.
#
# @param smtp_tls_session_cache_database
#   smtp_tls_session_cache_database. String. Default value: Undef.
#
# @param smtpd_tls_loglevel
#   smtpd_tls_loglevel. Integer. Default value: Undef.
#
# @param smtpd_tls_received_header
#   smtpd_tls_received_header. String. Default value: Undef.
#
# @param smtpd_tls_session_cache_timeout 
#   smtpd_tls_session_cache_timeout . String. Default value: Undef.
#
# @param smtpd_tls_mandatory_protocols
#   smtpd_tls_mandatory_protocols. String. Default value: Undef.
#
# @param smtp_tls_mandatory_protocols
#   smtp_tls_mandatory_protocols. String. Default value: Undef.
#
# @param smtpd_tls_protocols
#   smtpd_tls_protocols. String. Default value: Undef.
#
# @param smtp_tls_protocols
#   smtp_tls_protocols. String. Default value: Undef.
#
# @param smtpd_tls_ciphers
#   smtpd_tls_ciphers. String. Default value: Undef.
#
# @param smtp_tls_mandatory_ciphers
#   smtp_tls_mandatory_ciphers. String. Default value: Undef.
#
# @param smtp_tls_mandatory_ciphers
#   smtp_tls_mandatory_ciphers. String. Default value: Undef.
#
# @param smtp_tls_mandatory_exclude_ciphers
#   smtp_tls_mandatory_exclude_ciphers. String. Default value: Undef.
#
# @param smtp_tls_exclude_ciphers
#   smtp_tls_exclude_ciphers. String. Default value: Undef.
#
# @param enable_client_ssl
#   enable_client_ssl. Boolean. Default value: Undef.
#
# @param smtp_use_tls
#   smtp_use_tls. String. Default value: Undef.
#
# @param smtp_tls_note_starttls_offer
#   smtp_tls_note_starttls_offer. String. Default value: Undef.
#
# @param smtp_tls_loglevel
#   smtp_tls_loglevel. String. Default value: Undef.
#
# @param enable_sasl
#   .enable_sasl Boolean. Default value: Undef.
#
# @param smtpd_sasl_local_domain
#   smtpd_sasl_local_domain. String. Default value: Undef.
#
# @param smtpd_sasl_auth_enable
#   smtpd_sasl_auth_enable. String. Default value: Undef.
#
# @param broken_sasl_auth_clients
#   broken_sasl_auth_clients. String. Default value: Undef.
#
# @param smtpd_sasl_authenticated_header
#   smtpd_sasl_authenticated_header. String. Default value: Undef.
#
# @param enable_ldap_alias
#   enable_ldap_alias. Boolean. Default value: Undef.
#
# @param ldapaliases_server_host
#   . String. Default value: Undef.
#
# @param ldapaliases_search_base
#   . String. Default value: Undef.
#
# @param ldapaliases_scope
#   . String. Default value: Undef.
#
# @param ldapaliases_query_filter
#   . String. Default value: Undef.
#
# @param ldapaliases_result_attribute
#   . String. Default value: Undef.
#
# @param alias_maps
#   . String. Default value: Undef.
#
# @param alias_database
#   . String. Default value: Undef.
#
# @param myorigin
#   . String. Default value: Undef.
#
# @param relay_domains
#   relay_domains. Array. Default value: Undef.
#
# @param mydestination
#   mydestination. Array. Default value: Undef.
#
# @param relayhost
#   . String. Default value: Undef.
#
# @param mynetworks
#   . Array. Default value: Undef.
#
# @param sender_canonical_maps
#   . String. Default value: Undef.
#
# @param message_size_limit
#   . String. Default value: Undef.
#
# @param mailbox_size_limit
#   . Integer. Default value: Undef.
#
# @param recipient_delimiter
#   . String. Default value: Undef.
#
# @param inet_protocols
#   . String. Default value: Undef.
#
# @param inet_interfaces
#   . String. Default value: Undef.
#
# @param html_directory
#   . String. Default value: Undef.
#
# @param enable_milters
#   . Boolean. Default value: Undef.
#
# @param smtpd_milters
#   . Array. Default value: Undef.
#
# @param non_smtpd_milters
#   . Array. Default value: Undef.
#
# @param milter_protocol
#   . Integer. Default value: Undef.
#
# @param milter_mail_macros
#   . String. Default value: Undef.
#
# @param milter_default_action
#   . String. Default value: Undef.
#
# @param smtpd_client_restrictions
#   . Array. Default value: Undef.
#
# @param smtpd_helo_restrictions
#   . Array. Default value: Undef.
#
# @param smtpd_sender_restrictions
#   . Array. Default value: Undef.
#
# @param smtpd_recipient_restrictions
#   . Array. Default value: Undef.
#
# @param header_checks
#   . String. Default value: Undef.
#
# @param mime_header_checks
#   . String. Default value: Undef.
#
# @param enable_postscreen
#   . Boolean. Default value: Undef.
#
# @param postscreen_bare_newline_action
#   . String. Default value: Undef.
#
# @param postscreen_dnsbl_sites
#   . Array. Default value: Undef.
#
# @param postscreen_access_list
#   . Array. Default value: Undef.
#
# @param postscreen_blacklist_action
#   . String. Default value: Undef.
#
# @param postscreen_dnsbl_threshold
#   . Integer. Default value: Undef.
#
# @param postscreen_dnsbl_action
#   . String. Default value: Undef.
#
# @param postscreen_greet_banner
#   . String. Default value: Undef.
#
# @param postscreen_greet_action
#   . String. Default value: Undef.
#
# @param postscreen_pipelining_enable
#   . String. Default value: Undef.
#
# @param postscreen_pipelining_action
#   . String. Default value: Undef.
#
# @param postscreen_non_smtp_command_enable
#   . String. Default value: Undef.
#
# @param postscreen_non_smtp_command_action
#   . String. Default value: Undef.
#
# @param postscreen_bare_newline_enable
#   . String. Default value: Undef.
#
# @param mailbox_transport
#   . String. Default value: Undef.
#
# @param virtual_mailbox_domains
#   . Array. Default value: Undef.
#
# @param virtual_mailbox_base
#   . String. Default value: Undef.
#
# @param virtual_mailbox_maps
#   . String. Default value: Undef.
#
# @param virtual_uid_maps
#   . String(Integer?). Default value: Undef.
#
# @param virtual_gid_maps
#   . String(Integer). Default value: Undef.
#
# @param virtual_transport
#   . String. Default value: Undef.
#
# @param cyrus
#   . Boolean. Default value: Undef.
#
# @param policyd_spf
#   . Boolean. Default value: Undef.
#
# @param submission
#   . Boolean. Default value: Undef.
#
# @param smtps
#   . String. Default value: Undef.
#
class postfix (

  Array   $config_files,
  Array   $access_files,
  Array   $db_files,
  Array   $postfix_files_content,
  Array   $sasl_files,
  String  $postfix_files_directory,
  String  $sasl_files_directory,

  String  $service_name                         = 'postfix',
  Boolean $service_enable                       = true,
  String  $service_ensure                       = 'running',
  Boolean $service_manage                       = true,
  #Stdlib::Absolutepath $configuration_directory = '/etc/postfix/',
  String $configuration_directory = '/etc/postfix/',
  $sasl_files_full_directory = "${configuration_directory}/${sasl_files_directory}",
  $postfix_files_full_directory = "${configuration_directory}/${postfix_files_directory}",
  Optional[Array] $postfix_packages,

  ## templating
  Optional[String]  $mail_name = undef,
  Optional[String]  $smtpd_banner = undef,
  Optional[String]  $myhostname = undef,
  Optional[String]  $mydomain = undef,
  Optional[String]  $smtp_helo_name = undef,

  Optional[Boolean] $enable_debug = undef,
  Optional[String]  $debug_peer_list = undef,
  Optional[String]  $soft_bounce = undef,
  Optional[String]  $delay_warning_time = undef,

  Optional[Boolean] $enable_server_ssl = undef,
  Optional[String]  $smtpd_tls_cert_file = undef,
  Optional[String]  $smtpd_tls_key_file = undef,
  Optional[String]  $smtpd_use_tls = undef,
  Optional[String]  $smtpd_tls_session_cache_database = undef,
  Optional[String]  $smtp_tls_session_cache_database = undef,
  Optional[Integer] $smtpd_tls_loglevel = undef,
  Optional[String]  $smtpd_tls_security_level = undef,
  Optional[String]  $smtpd_tls_received_header = undef,
  Optional[String]  $smtpd_tls_session_cache_timeout = undef,
  Optional[String]  $smtpd_tls_mandatory_protocols = undef,
  Optional[String]  $smtp_tls_mandatory_protocols = undef,
  Optional[String]  $smtpd_tls_protocols = undef,
  Optional[String]  $smtp_tls_protocols = undef,
  Optional[String]  $smtpd_tls_ciphers = undef,
  Optional[String]  $smtp_tls_mandatory_ciphers = undef,
  Optional[String]  $smtp_tls_mandatory_exclude_ciphers = undef,
  Optional[String]  $smtp_tls_exclude_ciphers = undef,

  Optional[Boolean] $enable_client_ssl = undef,
  Optional[String]  $smtp_use_tls = undef,
  Optional[String]  $smtp_tls_note_starttls_offer = undef,
  Optional[Integer] $smtp_tls_loglevel = undef,

  Optional[Boolean] $enable_sasl = undef,
  Optional[String]  $smtpd_sasl_local_domain = undef,
  Optional[String]  $smtpd_sasl_auth_enable = undef,
  Optional[String]  $broken_sasl_auth_clients = undef,
  Optional[String]  $smtpd_sasl_authenticated_header = undef,

  Optional[Boolean] $enable_ldap_alias = undef,
  Optional[String]  $ldapaliases_server_host = undef,
  Optional[String]  $ldapaliases_search_base = undef,
  Optional[String]  $ldapaliases_scope = undef,
  Optional[String]  $ldapaliases_query_filter = undef,
  Optional[String]  $ldapaliases_result_attribute = undef,
  Optional[String]  $alias_maps = undef,
  Optional[String]  $alias_database = undef,

  Optional[String]  $myorigin = undef,
  Optional[Array]   $relay_domains = undef,
  Optional[Array]   $mydestination = undef,
  Optional[String]  $relayhost = undef,
  Optional[Array]   $mynetworks = undef,
  Optional[String]  $sender_canonical_maps = undef,
  Optional[String]  $message_size_limit = undef,
  Optional[Integer] $mailbox_size_limit = undef,
  Optional[String]  $recipient_delimiter = undef,
  Optional[String]  $inet_interfaces = undef,
  Optional[String]  $inet_protocols = undef,
  Optional[String]  $html_directory = undef,

  Optional[Boolean] $enable_milters = undef,
  Optional[Array]   $smtpd_milters = undef,
  Optional[Array]   $non_smtpd_milters = undef,
  Optional[Integer] $milter_protocol = undef,
  Optional[String]  $milter_mail_macros = undef,
  Optional[String]  $milter_default_action = undef,

  Optional[Array]   $smtpd_client_restrictions = undef,
  Optional[Array]   $smtpd_helo_restrictions = undef,
  Optional[Array]   $smtpd_sender_restrictions = undef,
  Optional[Array]   $smtpd_recipient_restrictions = undef,

  Optional[String]  $header_checks = undef,
  Optional[String]  $mime_header_checks = undef,

  Optional[Boolean] $enable_postscreen = undef,
  Optional[Array]   $postscreen_dnsbl_sites = undef,
  Optional[Array]   $postscreen_access_list = undef,
  Optional[String]  $postscreen_blacklist_action = undef,
  Optional[Integer] $postscreen_dnsbl_threshold = undef,
  Optional[String]  $postscreen_dnsbl_action = undef,
  Optional[String]  $postscreen_greet_banner = undef,
  Optional[String]  $postscreen_greet_action = undef,
  Optional[String]  $postscreen_pipelining_enable = undef,
  Optional[String]  $postscreen_pipelining_action = undef,
  Optional[String]  $postscreen_non_smtp_command_enable = undef,
  Optional[String]  $postscreen_non_smtp_command_action = undef,
  Optional[String]  $postscreen_bare_newline_enable = undef,
  Optional[String]  $postscreen_bare_newline_action = undef,

  Optional[String]  $mailbox_transport = undef,

  Optional[Array]   $virtual_mailbox_domains = undef,
  Optional[String]  $virtual_mailbox_base = undef,
  Optional[String]  $virtual_mailbox_maps = undef,
  Optional[String]  $virtual_uid_maps = undef,
  Optional[String]  $virtual_gid_maps = undef,
  Optional[String]  $virtual_transport = undef,

  Optional[Boolean] $cyrus = undef,
  Optional[Boolean] $policyd_spf = undef,
  Optional[Boolean] $submission = undef,
  Optional[Boolean] $smtps = undef,

  ) {

  anchor { 'postfix::begin': } -> class { '::postfix::install': } -> class { '::postfix::config': } ~> class { '::postfix::service': } -> anchor { 'postfix::end': }

}
